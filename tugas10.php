<!-- <?php
$nilai = 50;
if ($nilai >= 60) {
    echo "Nilai Anda $nilai, Anda Lulus";
} else {
    echo "Nilai Anda $nilai, Anda Gagal";
}
?> -->

<?php
echo "<PRE>\n";

$tinggi = isset($_GET['tinggi']) ? $_GET['tinggi'] : 5;

for ($baris = 1; $baris <= $tinggi; $baris++) {
    // Buat sejumlah spasi
    for ($i = 1; $i <= $tinggi - $baris; $i++) {
        echo " ";
    }
    // Tampilkan *
    for ($j = 1; $j <= (2 * $baris) - 1; $j++) {
        echo "*";
    }
    // Pindah baris
    echo "\n";
}

echo "</PRE>\n";
?>